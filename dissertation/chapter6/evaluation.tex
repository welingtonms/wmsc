\section{Metodologia}

O Estudo de Caso aqui apresentado segue, em linhas gerais, uma abordagem adaptada da versão apresentada em \citep{baxter2008qualitative}, devido à sua simplicidade de
descrição e compreensão.

O objetivo da avaliação aqui descrita é avaliar a percepção do usuário em relação à proposta de gerenciamento de privacidade de dados pessoais, em modelo centrado no
usuário, visando verificar o engajamento dos usuários através de um controle auxiliado e verificar se decisões informadas podem fazê-lo assumir ações mais responsáveis
ao expor seus dados.

Como a ideia é avaliar os índices propostos de auxílio a decisão, a Questão de Pesquisa definida para guiar o Estudo é a seguinte:

\begin{quote}
``\textbf{As pessoas são mais propensas a expor dados quando apoiadas por parâmetros quantitativos ?}''
\end{quote}

O caso proposto para estudo foi dividido em diferentes cenários, como atendimento médico, redes sociais, serviços públicos, pagamento, etc., para os quais foi listado um
conjunto de situações que poderiam abrigar, nos quais fossem disponibilizados serviços que dependessem do uso de dados pessoais. Um indivíduo então seria submetido a um
conjunto de cenários e suas ações, em cada um deles, deveria ser registrada para análise.

Para que o conjunto de cenários e a transição entre eles fosse coerente, foi necessário encontrar uma forma de descrevê-los, de modo que o usuário fosse conduzido
imperceptivelmente de um a outro, com a atenção minada pelo contexto, na qual a situação de exposição de dados seria embutida, sem criar nenhum tipo de alerta para o que
estava sendo feito realmente.

Com o auxílio de uma equipe composta por Engenheiros de Usabilidade e de \textit{Game Designers}, decidiu-se que o Estudo de Caso poderia ser conduzido no formato de uma
história interativa, na qual situações fossem propostas e decisões pudessem ser feitas, conduzindo o participante pelo experimento. Como embasamento teórico, foi
necessário estudar os princípios de \textit{Storytelling} e de \textit{Gamebooks}, para compor a execução do caso. Estes assunto serão abordados nas subseções
\ref{subsec:storytelling} e \ref{subsec:gamebooks} que se seguem.

\subsection{\textit{Storytelling}}
\label{subsec:storytelling}

De acordo com \citep{eisner2008narrativas}, desde os tempos remotos ``\textit{o ato de contar histórias está enraizado no comportamento social dos grupos humanos}''.
Inicialmente usadas como ferramenta para transmissão de conhecimento entre gerações, as histórias passaram a entreter, educar, ensinar comportamentos, discutir valores
ou satisfazer a curiosidade.

Esta técnica consiste na criação de uma história, constituída de um início e um fim, intermediados por uma sequência de eventos estruturados de modo coeso, incluindo um
ou mais personagens, um ambiente na qual os evento se desenrolam e um (ou mais) acontecimento principal que representa o clímax (ponto alto de tensão) da história.

De acordo com Will Eisner \citep{eisner2005narrativas}, o ritmo com que os eventos se sucedem, a forma como o problema ou a situação clímax é resolvida, as causas que
levam a ela, o efeito que produzem e o ferramental cognitivo utilizado, são importantes para uma experiência satisfatória na \textit{storytelling}. Para isso, é essencial
que o contexto proposto pela história seja conhecido o suficiente pelo leitor a fim de que se produza empatia e o permita partilhar das experiências dos personagens, sendo este
o objetivo final da história.

% Anterior à invenção da escrita, a forma de contar histórias (ou \textit{storytelling}, em inglês) era basicamente oral, combinada com gestos e expressões corporais. Foi
% com o advento da escrita que as histórias puderam perdurar e transcender gerações, culturas e civilizações \citep{eder2010life}. Entretanto, a transmissão das
% histórias sendo feita de modo estático nos impressos, influenciava a forma como era contada. Foi quando foram inseridas imagens, representadas de maneira simplista para
% facilitar sua assimilação, para dar maior expressividade à narração.
% 
% Nos dias atuais, o ``contar histórias'' pôde ser enriquecido com o auxílio da tecnologia, usando de recursos audiovisuais para prender ainda mais a atenção do leitor/ouvinte, criando a atmosfera idealizada pelo
% autor no momento da escrita.
% 
% Umas das formas mais interessantes de contar história é através da narração, na qual se fala sobre acontecimentos reais ou fictícios, com dados reais ou imaginários,
% evoluindo os acontecimentos a medida que se desenvolve o enredo. Independente se escrito em 1ª ou 3ª pessoa, uma narrativa tem a capacidade de transportar o leitor para
% uma realidade possivelmente inventada, fazendo o sentir-se como o personagem, ou um expectador próximo, do desenrolar da história.

% Dentro deste cenário, manter o interesse e a compreensão do leitor torna-se um desafio. Will Eisner \citep{eisner2005narrativas} diz que o ritmo com que os eventos se
% sucedem, a forma como o problema ou a situação clímax é resolvida, as causas que levam a ela, o efeito que produzem e o ferramental cognitivo utilizado, são importantes
% para uma experiência satisfatória de leitura. Para isso, é essencial que o contexto proposto pela história seja conhecido o suficiente pelo leitor a fim de que se produza
% empatia e o permita partilhar das experiências dos personagens, sendo este o objetivo final da história.

\subsection{\textit{Gamebooks}}
\label{subsec:gamebooks}

Um \textit{Gamebook} é, em uma definição adequada ao propósito deste trabalho, um livro tipicamente escrito na 2ª pessoa, no qual o leitor atua na história através de
escolhas que afetam o curso da narrativa \citep{game2012faq} e que oferecem uma história não contínua adequada ao contexto de uma \textit{storytelling} interativa.

Dos exemplos mais antigos que se têm registro, podem-se citar \textit{An Examination of the Work of Herbert Quain} \citep{borge1941sexamination}, de Jorge Luis Borges, e a
série \textit{Tutor Text} \citep{crowder1958arithmetic}.

Em \textit{An Examination of the Work of Herbert Quain} são apresentados vários trabalhos do autor Herbert Quain, como o romance \textit{April March}, constituído de
treze capítulos, formando nove histórias diferentes. Partindo-se do primeiro capítulo, as decisões escolhidas pelo leitor podem levá-lo a um dos três próximos capítulos
e, cada um destes, levam a um dos três capítulos subsequentes \citep{kistler2011full}.

A série de livros educacionais \textit{Tutor Text}, permitiam que os alunos pudessem aprender sem a necessidade da presença de um professor. Questões de múltipla escolha
direcionavam para páginas diferentes de acordo com a resposta escolhida. No caso de uma resposta incorreta, o aluno seria levado para uma página que explicava o porque
aquela resposta estava errada; quando a resposta estivesse certa, uma página com mais informações seria indicada, incluindo as próximas questões.

% Atualmente existem três categorias de \textit{gamebooks}; a primeira, histórias de enredo ramificado, requerem que o leitor faça as escolhas para dar continuidade a história, levando a um dos finais previsto de acordo com as
% escolhas feitas \citep{game2012faq}. Um exemplo desta categoria é a série \textit{Choose Your Own Adventure}. Criada por Edward Packard, a série era voltada para crianças
% entre 10 a 14 anos; a cada conjunto de páginas, o leitor tinha duas ou três opções que o levavam a uma nova situação na história, culminando em um dos 40 finais
% possíveis. O conceito foi tão marcante para a época, ficando entre os 6 mais lidos, que a série foi divulgada inclusive no jornal americano \textit{The Day}, em 10 de
% outubro de 1981 \citep{kraft1981he}.
% 
% A segunda categoria, é uma aventura individual do tipo \textit{Role Playing Game} - mais conhecido pela sigla RPG - que combina o enredo ramificado às
% regras de RPG, sem a necessidade da figura do \textit{gamemaster}\footnote{Em RPG, o \textit{gamemaster} é a pessoa que atua como organizador, consultor para perguntas relacionadas à
% regras, arbitro e moderador, que controla aspectos do jogo, criando ambientes e situações nos quais os jogadores interagem}, auxiliado por manuais avulsos
% \citep{game2012faq}. A séria \textit{Tunnels \& Trolls}, de Ken St. Andre, representante desta categoria, foi publicada em 1975 como uma alternativa mais acessível ao
% concorrente \textit{Dungeons and Dragons}, trazia uma ``\textit{concepção baseada no The Lord of The Rings}, heróis conhecidos e alguns vilões.
% 
% Finalmente, a terceira e última categoria são os \textit{gamebooks} de aventura, na qual se usa o enredo ramificado com um sistema de \textit{role playing} único do livro
% (ou série). A série \textit{Fighting Fantasy}, representante desta categoria, criada por Steve Jackson e Ian Livingston, em 1982.
% 
% A forma como será utilizada neste trabalho, encaixa-se na primeira categoria.

Na forma como foi utilizada neste trabalho, a técnica foi empregada no formato de uma história de enredo ramificado \citep{game2012faq}, atribuindo ao leitor as escolhas
para sua continuidade, levando a um dos finais previstos.


\subsection{Por Que \textit{Storytelling} e \textit{Gamebook}?}
\label{subsec:why-storytelling-and-gamebooks}

A princípio, pensou-se em testar a proposta deste trabalho através de um questionário, no qual, os participantes seriam questionados sobre sua percepção da utilidade das
funções (consideradas para avaliação) do \textbf{\textit{Go!SIP}}, sem ou após o uso, em determinada situação de exposição de privacidade. 

Apesar dos questionários serem vistos como uma forma rápida de obtenção de opinião, serem objetivos, obtendo respostas em formato padronizado, caso seja aplicado
posterior ao experimento, alguns detalhes da experiência podem ser esquecidos. Além disso, se fosse necessário uma quantidade razoavelmente grande de perguntas, o
interesse do usuário poderia se perder ao longo do preenchimento, tornando-se um risco potencial à análise dos resultados. Finalmente, os participantes poderiam deixar de
revelar alguma informação, ou poderiam achar que seriam penalizados caso fornecessem sua real opinião \citep{milne1999questionnaires}.

Como o uso de questionários trazia em si o risco de que, a forma como os usuários atuariam nos cenários descritos poderia não ser próximo ao que fariam no mundo real, a
função de \textit{storytelling} no contexto deste trabalho foi estratégica, visando minimizar este problema. Era necessário expor as pessoas a cenários cotidianos,
eventualmente futurísticos, no qual se propusessem situações em que seriam impelidas a fornecer seus dados pessoais. Decidiu-se utilizar uma história, em forma de
narrativa, organizada como um \textit{gamebook} e adicionando uma camada de ludicidade à proposta, de modo que as pessoas se sentissem livres para expressar suas decisões
da forma mais sincera possível.

Agregando as duas técnicas, fez-se possível extrair as informações necessárias para o processo de avaliação, através de um questionário-jogo, no formato de história,
trabalhando a experiência de forma intrinsecamente lúdica.






