\section{Execução da Avaliação}

Após decidir que o Estudo de Caso seria materializado através de \textit{storytelling}, organizado como um \textit{gamebook}, iniciaram-se as atividades rumo à execução
da experiência, partindo da concepção da história, definição do perfil esperado de participantes, ao desenvolvimento da aplicação até sua disponibilização. Cada uma
dessas etapas são exploradas das subseções que se seguem.

\subsection{A Criação da História}

Para a criação da história, os mesmos pesquisadores envolvidos ao longo do desenvolvimento deste trabalho foram convidados a propor situações cotidianas isoladas, que
poderiam se passar em um contexto urbano, nas quais seria necessário expor algum dado pessoal.

Assim que a lista de situações foi criada, um enredo foi construído a fim de conectá-las. Antes de partir para fase de implementação, um \textit{Game Designer} foi
convidado a validar a história, destacando pontos que poderiam ser melhorados, de modo que se pudesse envolver ao máximo o usuário e extrair dele as informações
necessárias. Realizou-se vários ciclos de revisão  nesta etapa; cada revisão tinha como objetivo, além de avaliar a ludicidade da história e a adequação aos interesses da
pesquisa, certificar-se de que cada situação iria impactar em alguma ação futura, criando a noção de causa-efeito. Além disso, os finais possíveis deveriam ter conteúdo
condizente com a trajetória selecionada, expostos de forma possivelmente cômica, para concluir a experiência de participação que trouxesse à memoria o aspecto lúdico
intencionado.

O grande desafio da etapa de criação da história foi, na verdade, desenvolver as ramificações da história nos pontos de decisão, nos quais as opções seriam apresentadas
ao participante, e conduzir cada uma das continuações de forma coerente, levando a um final interessante, que retratasse as escolhas feitas.

Depois das inúmeras rodadas de revisão, a história foi finalmente concluída e pode ser conferida no Apêndice~\ref{app:history}, organizada de acordo com os Passos, ou
trechos, alcançados por meio das escolhas feitas durante sua evolução. 

Resumidamente, o participante acabou de chegar das Terras do Mar Prestos para o local onde a história acontece: uma cidade fictícia, chamada Privus, localizada em um país
chamado Secreta. Logo que chega na cidade, o personagem passa por um breve questionário no guichê de imigração, onde é apresentado um contexto geral sobre a cidade. A
partir daí, várias opções são apresentadas, entre passear pela cidade, pagar contas, usar ou não serviços de localização, redes sociais, etc., ações comuns pensadas de
forma que pudessem fazer sentido em uma Cidade Inteligente.

\subsection{Ferramentas}

Para a criação do \textit{gamebook} interativo foi desenvolvida uma aplicação \textit{Web}, para facilitar o acesso e a comodidade dos participantes, além da
familiaridade com as tecnologias utilizadas.

A aplicação Web foi desenvolvida em Python, usando um \textit{framework} chamado Django\footnote{\url{https://www.djangoproject.com/}}, em sua versão 1.6.1. Utilizou-se o
\textit{Eclipse Kepler} como \textit{Integrated development environment} (IDE). Para persistência, foi utilizado o PostgreSQL\footnote{\url{http://www.postgresql.org/}}.
A interface foi construída usando Bootstrap\footnote{\url{http://getbootstrap.com/}} 3.2, CSS, HTML e javascript.

Como dois desenvolvedores foram envolvidos, o código foi disponibilizado via repositório privado no \textit{BitBucket}\footnote{\url{https://bitbucket.org/}}, usando o
sistema de controle de versão \textit{open source} Git\footnote{\url{http://git-scm.com/}}.
  
Para disponibilizar a aplicação, utilizou-se a infraestrutura \textit{Amazon Web Services} (AWS), com uma instância gratuita da \textit{Amazon Elastic Compute Cloud}
(EC2).


\subsection{Perfil de Participação}

Para participação do Estudo de Caso, discutiu-se a possibilidade de incluir pessoas de diferentes faixas etárias, com conhecimento básico de informática, o suficiente
para informar suas escolhas na história. Entretanto, ao longo do desenvolvimento, notou-se que seria necessário um entendimento básico dos serviços que seriam fornecidos,
simulados ou não, durante a história, com familiaridade suficiente para entender e decidir sua utilização, já que se tentou assemelhar ao máximo o nome e a proposta dos
serviços do mundo real.

Com isso, entendeu-se que o melhor perfil para participar seriam pessoas envolvidas cotidianamente com o mundo de Tecnologia da Informação, habituados aos serviços
representados e discussões abordadas neste trabalho. Sendo assim, a aplicação foi disponibilizada apenas para
os estudantes de pós gradução do Centro de Informática da Universidade Federal de Pernambuco, para os colaboradores do Centro de Estudos e Sistemas Avançados do Recife e
para alguns alunos do Departamento de Computação da Universidade Federal de São Carlos, \textit{campus} Sorocaba.

\subsection{Desenvolvimento}

Cada etapa da história foi separada em páginas diferentes, que eram acessadas à medida que as opções que representavam eram escolhidas.

O estágio que possui maior expressão da proposta do Go!SIP é o \textbf{Passo 2c}, no qual o participante pode configurar o seu \ac{IPI}. Durante a implementação,
percebeu-se que ao invés representar este índice da forma como foi originalmente descrito - com valores fixos 0 (secreto), 50 (fechado) e 100 (público) - seria mais
flexível e de mais fácil entendimento se a faixa de valores fosse contida em um intervado discreto. Decidiu-se que a melhor forma de traduzir a semântica da
métrica para o participante seria a seguinte: ``\textit{De 1 a 100, o quanto seus dados \ldots importam para você?}''. Para facilitar a compreensão
deste valor, conforme o participante deslizava um \textit{slider} escolhido para este propósito, um \textit{tooltip} era exibido com uma categorização textual, conforme
segue:

\begin{itemize}
  \item \textbf{\([ 1, 20[\)}:  ``Não me importo'';
  \item \textbf{\([20, 40[\)}:  ``Quase não me importo'';
  \item \textbf{\([40, 60[\)}:  ``Me importo pouco'';
  \item \textbf{\([60, 80[\)}:  ``Me importo'';
  \item \textbf{\([80, 100[\)}:  ``Me importo muito''.
\end{itemize}

A definição do \ac{IPI} foi feita a nível de perfil, por questões de facilidade de configuração; o valor padrão de \ac{IPI} é 100, indicando importância máxima. Considera
se como perfil, um conjunto de atributos de um indivíduo, agrupados convenientemente para um determinado contexto. Na implementação atual são considerados 6 perfis,
pensados para explorar o máximo de aspectos possíveis durante o estudo, como descrito abaixo:

\begin{itemize}
  \item \textbf{Dados Pessoais}, inclui nome e email do indivíduo;
  \item \textbf{Dados Financeiros}, inclui dados fictícios sobre a conta bancária do indivíduo e os lançamentos em seu cartão de crédito;
  \item \textbf{Dados de Consumo}, inclui o histórico fictício de consumo do indivíduo, como data, local de compras, o que foi comprado e qual o valor;
  \item \textbf{Dados de Localização}, inclui o histório fictício de localização do indivíduo, supostamente coletado em intervalos de tempo;
  \item \textbf{Dados Médicos}, inclui data de nascimento e sexo do indivíduo, bem como seu histórico fictício de sinais vitais, como taxa respiratória, temperatura
  corporal e pressão;
  \item \textbf{Dados de Relacionamentos}, inclui a lista de amigos do indivíduo. 
\end{itemize} 

A taxa de degradação ($d$) de dados dos atributos presentes no perfil foi definida de modo fixo, não sendo exposta ao participante de forma alguma e foram estabelecidos,
ora a nível de atributos (mesmo que sob uma abstração, como conta bancária, por exemplo), ora a nível de perfil. Os seguintes valores de taxas de degradação foram
considerados:

\begin{itemize}
  \item \textbf{Nome} e \textbf{Email}, $d = 0$, nível de atributo; 
  \item \textbf{Conta bancária}, $d = 3,333$, variação por dia, nível de atributo;
  \item \textbf{Cartão de crédito}, $d = 0.139$, variação por hora, nível de atributo;
  \item \textbf{Localização}, $d = 0.139$, variação por hora, nível de perfil;
  \item \textbf{Relacionamento}, $d = 3,333$, variação por dia, nível de perfil;
  \item \textbf{Dados médicos}, $d = 0,555$, variação por semestre, nível de perfil;
  \item \textbf{Dados de consumo}, $d = 3,333$, variação por dia, nível de perfil;
\end{itemize}

O cálculo da precisão de cada um dos elementos descritos acima levou em consideração o tempo decorrido desde o momento que o usuário iniciou sua participação no estudo,
cadastrando/autenticando-se na aplicação. Deste modo, o dado vai se depreciando, ainda que vagarosamente, durante a experiência do participante.

A cada escolha de exposição do participante, calcula-se o \ac{IE}, baseado no valor atual do \ac{IPI} (já que o participante pode alterar esta informação a
qualquer tempo) e da precisão. Para efeitos de histórico, registra-se o \ac{IPI} e o \ac{IE} de cada perfil envolvido juntamente com o contexto na qual se
baseia a escolha, o provedor de serviço e o \textit{timestamp} da transação. Além disso, para cada passo da história é registrado qual foi a opção escolhida pelo
participante.

Sempre que algum serviço é consumido o cálculo de exposição é executado, conforme explicado, e exibido para o participante de forma visual, gráfica, textual e
numericamente, como mostra a Figura~\ref{fig:service-fakebook}. Para cada perfil é exibido uma barra horizontal que varia de tamanho de acordo com o risco calculado,
colorida, variando de verde - para exposição mínima - a vermelho - para exposição máxima; junto à essa barra, também é mostrado o valor bruto de exposição calculado e uma
categorização como segue:

\begin{itemize}
  \item \textbf{\([ 1, 20[\)}:  ``Risco baixíssimo'';
  \item \textbf{\([20, 40[\)}:  ``Risco baixo'';
  \item \textbf{\([40, 60[\)}:  ``Risco médio'';
  \item \textbf{\([60, 80[\)}:  ``Risco alto'';
  \item \textbf{\([80, 100[\)}:  ``Risco altíssimo''.
\end{itemize}

Essas informações deveriam ser exibidas sempre que o usuário tivesse que fazer a aquisição de um serviço, sendo bem destacadas na tela e referenciadas no texto
de descrição. Entretanto, como a ideia era verificar se métricas quantitativas faziam diferença na decisão de exposição de dados, implementou-se um mecanismo
que, na primeira vez que o participante entrava na aplicação, era determinado se ele ia ou não ter acesso ao \textit{score} e às métricas; a forma como esta
característica foi implementada garante que a diferença entre a quantidade de participantes tendo acesso ou não às métricas seria de no máximo um (1), i.e., idealmente
seriam quantidades iguais. Para os participantes que não tinham acesso às metricas, nenhuma referência literal à privacidade foi feita, nem mesmo era informado que
este seria o assunto sendo observado no Estudo, nem a barra horizontal, a categorização textual ou o valor de exposição era exibido.

Para manter o foco nas métricas, não foram oferecidos múltiplos provedores de serviço, tanto por questões de simplicidade de implementação, quanto para não interferir
na percepção da exposição dos dados; quanto maior a carga cognitiva necessária para participar do experimento, menos efetivo ele seria e maior a dificuldade em convencer
as pessoas a participar.

A utilização dos perfis de dados por parte dos serviços pode ser encontrada no Apêndice~\ref{app:services}. Além dos serviços, algumas situações
específicas também utilizam o perfil Dados Financeiros do participante; a saber, na parte da história que descreve a compra de um carro, quando no pagamento da conta de
energia e do pedido no Burger Queen.

Para registrar o impacto das ações de forma quantitativa durante o jogo, um \textit{score} é exibido no canto direito da tela, apresentando os seguintes indicadores:

\begin{itemize}
  \item Dinheiro
  \item Risco
  \item Influência
\end{itemize}

O indicador Dinheiro é registrado com uma unidade monetária fictícia (P\$), inicialmente apresenta o valor 20.000, como um benefício do governo local para o
imigrante, para que se estabelecesse de modo confortável na cidade. Este valor é impactado sempre que o participante paga algum serviço ou produto; o indicador Risco está
fora do controle do participante e foi estabelecido previamente, de forma empírica e \textit{hard coded}, e é um valor para indicar o quanto o usuário já se expôs ao
longo da história, cujo valor incrementado depende da situação (este índice não representa o Índice de Exposição); o terceiro indicador que compõe o \textit{score} é a
Influência, que representa a reputação virtual do participante, e é incremementada sempre que alguma interação social é realizada.

Nos serviços disponibilizados na aplicação, o impacto no \textit{score} foi atribuído arbitrariamente, conforma mostra a Tabela~\ref{tab:serviceImpact}:

\begin{table*}[ht] \centering \small
\caption{Impacto dos Serviços no \textit{score} do participante}
\begin{tabular}{ |l|c|c|c|}
\hline
\textbf{Serviço} & \textbf{Risco} & \textbf{Dinheiro (P\$)} & \textbf{Influência}\\ 
\hline\hline
Gugou Maps & +5 & 2,00 & 0 \\ \hline
Fakebook & +10 & 0,00 & +10 \\ \hline
Twistter & +10 & 0,00 & +10 \\ \hline
Check-In & +5 & 0,00 & +10 \\ \hline
Smart Meter & +5 & 2,00 & 0 \\ \hline
Health Monitor & +5 & 8,00 & 0 \\ \hline
Health Assist & +5 & 0,00 & 0 \\ \hline
\end{tabular}
\label{tab:serviceImpact}
\end{table*}

Assim como os serviços, alguns passos da históra implicam na utilização de algumas outras funcionalidades, como pagamento, execução de alguma tarefa, fazer
um \textit{tour} com os amigos, também interferem no \textit{score}, como mostra a Tabela~\ref{tab:stepsImpact}.

\begin{table*}[ht] \centering \small
\caption{Impacto dos Passos no \textit{score} do participante}
\begin{tabular}{ |l|c|c|c|l|}
\hline
\textbf{Serviço} & \textbf{Risco} & \textbf{Dinheiro (P\$)} & \textbf{Influência} & \textbf{Situação}\\ 
\hline\hline
Passo 4c & 0 & 0.0 & +5 & Participante escolhe fazer \textit{tour} com seus amigos\\ \hline
Passo 6c & +15 & (76,5\%) & +5 & Participante compra o carro e participa da pesquisa\\ \hline
Passo 6d & +10 & (90\%) & +5 & Participante compra o carro sem participar da pesquisa\\ \hline
Passo 8b & 0 & 0 & -15 & Participante se recusa a pagar a conta de energia\\ \hline
Passo 9a & 5 & 150,00 & 0 & Participante paga a conta de energia com cartão\\ \hline
Passo 9b & 0 & 150,00 & 0 & Participante paga a conta de energia com dinheiro\\ \hline
Passo 9c & 5 & (Variável) & 0 & Participante paga a conta no Burger Queen com cartão\\ \hline
Passo 8b & 0 & (Variável) & 0 & Participante paga a conta no Burger Queen com dinheiro\\ \hline
\end{tabular}
\label{tab:stepsImpact}
\end{table*}

Ao longo da história diversos formulários são apresentados. Porém, com exceção do formulário apresentado no Passo 2c (também acessado através do menu \textbf{Meus
Dados}), todos os formulários são inertes, ou seja, nenhum deles salva ou registra os dados informados. A única coisa que é registrada é que o participante manifestou a
intenção de fazê-lo. Isso permitiu que apenas os dados relevantes para a análise fossem persistidos.

Finalmente, em nome da ludicidade, vários recursos foram utilizados para deixar algumas experiências dentro da história mais interessante. Notificação de serviço são
exibidas a medida o usuário atinge um ponto específico de leitura e, em alguns casos, sons também foram utilizados; na etapa da história em que ocorre um problema de
saúde, além da notificação, a tela fica embaçada aleatóriamente, dando um apelo visual à situação. Para implementação desses recursos foram utilizados CSS3, HTML5 e
javascript.

\begin{figure}[t]
\caption{Exemplo da tela do Go!SIP}
\includegraphics[width=\textwidth, frame]{gosip-screen}
\centering
\label{fig:gosip-screen}
\end{figure}

A Figura~\ref{fig:gosip-screen} apresenta uma das telas da aplicação desenvolvida, com destaque para os serviço habilitados à esquerda e o \textit{score} do participante
à direita. Ao centro da tela, pode-se ver um dos passos da subrotina de Navegação, na qual o participante deve escolher para onde quer seguir rumo ao seu objetivo,
destacado na parte superior do mapa.

A aplicação ficou disponível para utilização durante duas semanas e foi divulgada verbalmente e através de emails para o público alvo. Durante este período, as dúvidas e
discussões que iam surgindo foram anotadas a fim de fomentar a discussão dos resultados do trabalho.
