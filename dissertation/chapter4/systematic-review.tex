\section{Revisão Sistemática}
\label{sec:systematic-review}

De acordo com \citep{keele2007guidelines}, uma revisão sistemática é um meio de identificar, avaliar e interpretar todo material relevante para um questionamento, tópico,
ou fenômeno de interesse em uma pesquisa, sumarizando evidências sobre uma tecnologia ou assunto específico, identificando problemas em aberto na área em estudo e
fornecendo subsídios para o posicionamento de novas pesquisas.

A revisão realizada no contexto deste trabalho, buscou levantar mecanismos de privacidade aplicados a ambientes inteligentes, na busca de requisitos gerais satisfeitos
nas propostas encontradas, rumo a uma convergência para proposta de um mecanismo de privacidade específico para um contexto maior: uma \ac{CI}. O objetivo era reunir
quaisquer trabalhos que tratassem de privacidade para algum tipo de ambiente explicitamente declarado como inteligente.

\subsection{Metodologia}

Nesta subseção será descrita a estratégia de busca adotada e o processo de refinamento dos resultados. Os passos foram guiados pela recomendações disponíveis em
\citep{keele2007guidelines}. Para orientar os resultados esperados pela revisão, as seguintes questões de pesquisa foram estabelecidas:

\begin{enumerate}[label=\bfseries Questão \arabic*:]
  \item Quais abordagens de gestão de privacidade já foram propostos (academia/indústria) dentro do contexto de smart X?
  \item De que forma as arquiteturas para cidades inteligentes já existentes implementam gestão de privacidade de dados dos cidadãos?
%   \item Que métodos de representação computacional do mecanismo de decisão humano para expor/proteger dados já foram propostos e/ou implementados?
\end{enumerate}

A primeira pergunta visava recuperar trabalhos que reportem o desenvolvimento de \textit{frameworks} ou arquiteturas focadas na provisão de privacidade em algum contexto
explicitamente declarado como inteligente; como o foco final do presente trabalho é desenvolver um \textit{framework} de privacidade para \ac{CI}, a segunda pergunta visa
identificar trabalhos que tenham denvolvido ou proposto uma solução no mesmo sentido.
%; a terceira pergunta, direciona o foco futuro do framework a ser proposto, buscando métodos computacionais de implementação de cognição.

\subsubsection{Processo de Busca}

O processo de busca seguiu a mesma metodologia empregada em \citep{keele2007guidelines}, \citep{chen2009variability} e \citep{khurum2009systematic}.

Para realização das busca, foram elencadas as principais bibliotecas digitais de literatura científica - \textit{IEEE Xplore}, \textit{ACM Digital Library},
\textit{CiteSeerX}, \textit{ScienceDirect}, \textit{SpringerLink} e \textit{Scopus} - e, na busca de soluções propostas na índustria, utilizou-se o repositório de
patentes do \textit{World Intellectual Property Organization} (WIPO).
Devido à variação na sintaxe adotada pelos motores de busca \citep{chen2009variability}, os termos de busca utilizados tiveram que ser adaptados, mantendo-se equivalentes
seu valor lógico e semântico. Os termos de busca utilizados para cada uma das bibliotecas pode ser encontrados no Apêndice~\ref{app:sr-reposisitories}.

A Tabela~\ref{tab:sr-repositories-quantity} mostra a quantidade de trabalhos encontrados para cada uma das fontes de dados utilizadas.

\begin{spacing}{0.1}
\begin{table}[ht]
\centering
\caption{Quantidade de trabalhos retornados por biblioteca digital }
\label{tab:sr-repositories-quantity}
\begin{tabular}{|p{3cm}|c|c|}
    \hline
    \textbf{Fonte de dados}  &   \textbf{$^\sharp$ Q1} &   \textbf{$^\sharp$ Q2}\\%&   \textbf{$^\sharp$ Q3} \\
    \hline\hline
    %     ACM & 379 & 21 & 3\\
    ACM & 379 & 21\\
    \hline
    %CITESEERX & 139 & 0 & 8\\
    CITESEERX & 139 & 0\\
    \hline
    %IEEE & 686 & 81 & 4\\
    IEEE & 686 & 81\\
    \hline
%     ScienceDirect & 59 & 0 & 1\\
    ScienceDirect & 59 & 0\\
    \hline
    %Scopus & 822 & 109 & 28\\
    Scopus & 822 & 109\\
    \hline
    %SpringerLink & 57 & 52 & 837\\
    SpringerLink & 57 & 52\\
    \hline
    %WIPO (Patentes) & 32 & 41 & 6\\
    WIPO (Patentes) & 32 & 41\\
    \hline
\end{tabular}
\end{table}
\end{spacing}

\vspace{1 mm}

A qualidade dos motores de busca pode influenciar no número de trabalhos identificados primariamente, conforme discutido em \citep{chen2009variability}. Sendo assim, caso
algum outro termo tenha sido utilizado que não os especificados, alguns trabalhos podem não ter sido incluídos.

\subsubsection{Processo de Seleção}

Nesta revisão foram selecionadas apenas trabalhos que mencionavam tratar de privacidade para algum tipo de ambiente explicitamente declarado inteligente. Foram envolvidos
no processo 5 pesquisadores, sendo 2 PhDs, 1 doutorando, 1 MSc e 1 mestrando.

A Exclusão por Título, consistiu na identificação de trabalhos cujo título sugeria a descrição de um \textit{framework}/arquitetura/modelo de
gerenciamento de privacidade, aplicado a algum contexto inteligente, eventualmente específico para cidades inteligentes; no filtro Exclusão por Duplicação, foram
removidos da lista os trabalhos que apareceram em mais de um motor de busca; na Exclusão por Resumo, foram ignorados os trabalhos que, apesar do título adequar ao escopo
da revisão, a descrição do trabalho não condizia com as expectativas levantadas pelo título; para os trabalhos que o resumo gerou dúvida sobre a adequação, o filtro
Adequação ao Contexto da Pesquisa foi utilizado, através da leitura da introdução, \textit{core} da proposta e conclusão. Nos casos em que mais de um trabalho reportava a
mesma proposta, manteve-se o mais completo entre eles.

Ao final, puderam ser acessados 63 trabalhos acadêmicos. Não foram encontradas patentes de acordo com o interesse da revisão. Isso provavelmente indica a necessidade de participação/interesse da indústria no assunto.

O resultado da aplicação de cada filtro está representado na Tabela~\ref{tab:sr-repositories-filters}.

Na seção a seguir serão estudados diferentes soluções de gerenciamento de privacidade para contextos inteligentes encontradas na revisão, destacando os principais requisitos que visam atender.

\begin{spacing}{0.7}
\begin{table}[ht]
\centering
\caption{Filtros aplicados no processo de revisão}
\label{tab:sr-repositories-filters}
\begin{tabular}{|p{6cm}|c|c|}
    \hline
    \textbf{Filtro/Questão} &  \textbf{Q1} &   \textbf{Q2}\\%&   \textbf{Q3}\\
    \hline\hline
    Relevantes & 2174 & 304\\%& 887\\
    \hline
    Exclusão por Título & 328 & 37\\%& 28\\
    \hline
    Exclusão por Duplicação & 238 & 34\\%& 24\\
    \hline
    Exclusão por Resumo & 77 & 19\\%& 17\\
    \hline
    Adequação ao contexto da pesquisa & 65 & 19\\%& 13\\
    \hline
    Acessados & 45 & 18 \\%& 13\\
    \hline
\end{tabular}
\end{table}
\end{spacing}
\vspace{1 mm}
