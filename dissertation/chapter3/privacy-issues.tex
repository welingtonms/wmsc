\section{A Problemática}
\label{sec:the-problem}

Quando se pensa em pessoas, em cidadãos, como sensores em sua máxima extensão, na qual interações de quaisquer natureza assumem importância enquanto passíveis de
registro, não são poucas as oportunidades em que isso se apresenta como uma alternativa mais que viável, tanto passiva - sem participação explícita ou consentimento -
quanto ativamente, de livre e espontânea vontade. 

Comumente, a atuação como sensores se dá através de dispositivos móveis, habilitados para uso de Internet, porém, existem outras formas de interação que transformam
nossos dados e nossas ações, em informação de utilidade para algum contexto/domínio. Como exemplo, podem-se citar: câmeras de vigilância e demais sistemas de
monitoramento de ambientes físicos; \textit{smartphones} enviando informações de localização para os provedores de rede, compartilhando informações de diferentes
naturezas (imagem, vídeo, texto) através de aplicações; registros de sistemas de pagamento de cartões de crédito e débito, com quantidade de dinheiro gasto e lojas
visitadas; dados biométricos, como impressões digitais ou fotografias, usadas por exemplo em academias, sem nenhum comprometimento de manutenção de sigilo (em contextos
que requerem um abordagem mais sofisticada inclui-se correspondência de DNA, ou \textit{DNA matching}, e reconhecimentos facial e de voz), redes sociais, interações com
ambientes inteligentes (viabilizados pela \ac{IoT}) \citep{EU-2011}. Também podem-se citar os próprios serviços disponíveis na Web 2.0, que permitem aos usuários criarem e
compartilharem, voluntariamente, grandes quantidades de dados pessoais sobre eles mesmos, amigos e familiares.

Além dos dados e respectivos contextos já mencionados, considerando uma escala massiva, o monitoramento contínuo, o efeito incremental (como chamado em
\citep{tene2012big}, devido ao fato de, uma vez o dado exposto \textit{online}, torna-se quase impossível recuperá-lo ou excluí-lo), a capacidade de análise sofisticada e
a forma como os dados são armazenados atualmente, fazem com que as informações sejam cada vez mais granulares, reveladoras, permitindo que as corporações consigam
extrair mais informações sobre os indivíduos aos quais os dados se referem, ganhando competitividade sobre a concorrência e vantagens injustas sobre os consumidores;
concomitantemente, também podem-se levantar questionamentos a respeito dos processos de tomada de decisão automatizados, que imputam as decisões sobre a vida de um
indivíduo - tais como avaliação de crédito, perspectiva de trabalho e elegibilidade para a cobertura de seguro ou benefícios - para métodos automatizados baseados em
algoritmos e inteligência artificial \citep{rubinstein2012big}.

Uma pesquisa realizada na União Europeia, denominada \textit{Eurobarometer}, tinha como objetivo compreender o entendimento dos europeus com relação à exposição de dados
pessoais, sua consciência de como estas informações podem ser armazenadas para futura análise, suas preocupações em relação a esses novos usos de seus dados pessoais,
suas formas de proteger esses dados e suas expectativas em relação à regulamentação da proteção aos dados.

Parte dos entrevistados reconhecia que não há alternativa além de expor os dados se o que se quer é obter produtos ou serviços. Quando questionados sobre a
responsabilidade do manuseio seguro dos dados em redes sociais e/ou sites de compartilhamento, a maioria dos internautas (74\%) achavam que deviam ser eles mesmos os
responsáveis, dado que lhes fossem disponibilizados mecanismos para fazê-lo. Quando concedida a oportunidade de nomear uma segunda entidade a quem responsabilizar, os
resultados mencionam redes sociais ou sites de compartilhamento (73\%); as autoridades públicas foram muito menos citadas (45\%), apesar de reconhecer que sanções para
violações nos direitos de proteção aos dados, impondo uma multa em empresas que fizerem o uso de dados pessoais sem conhecimento prévio, deveriam ser a principal
prioridade dessas entidades \citep{eurobarometer2011}.

Em novembro de 2012, uma pesquisa realizada no Brasil pela FutureSight, a pedido da GSMA, uma representante dos interesses da indústria mundial de comunicação através de
dispositivos móveis, agregando aproximadamente 800 operadoras de serviços de telefonia móvel e mais de 200 corporações do ecossistema móvel, incluindo fabricantes,
indústria de \textit{software}, equipamentos, geradores de conteúdo digital – como ela mesmo se descreve – buscou estudar meios de prover aos usuários formas
contextualizadas e amigáveis de gerenciamento de informações e privacidade em dispositivos móveis \citep{GSMA-2012}. O propósito geral era entender as preocupações do
público brasileiro sobre privacidade e como essas preocupações influenciavam suas atitudes, frente ao uso de serviços e aplicações móveis (na Internet), com o intuito de
desenvolver uma experiência de privacidade efetiva e consistente, ajudando usuários a se tornarem familiarizados com a forma que gerenciam sua privacidade em dispositivos
móveis.

Quando questionados sobre suas preocupações com relação à privacidade de seus dados, 86\% dos usuários manifestaram essa preocupação quando acessavam a
Internet ou aplicações de um dispositivo móvel. Entretanto, desse percentual, 66\% estariam dispostos a continuar a utilização, independente do risco, e 32\% continuariam
a utilização se pudessem sentir que suas informações estariam em segurança.

Dos entrevistados, 81\% se consideravam seletivos na decisão de para quem exporiam seus dados e, 2 em cada 3 usuários, verificavam as informações que uma aplicação quer
acessar antes realizar a instalação. Apesar de manifestar essa consciência de escolha, 51\% assumiram concordar com os termos de privacidade sem lê-los e, desse
percentual, 74\% justificaram que não o fazem porque os termos são muito extensos.
% 88\% de todos os usuários móveis manifestaram preocupação de que as aplicações poderiam coletar suas informações sem consentimento. 

Um dos tipos de informação mais coletados pelas aplicações, a localização, também foi assunto da pesquisa. Dentre os usuários que utilizavam serviços geolocalizados, 92\%
disseram que gostariam de ser perguntados sobre a permissão de compartilhamento de sua localização; além disso 78\% manifestaram preocupação com o acesso de terceiros à
sua localização e 55\% acreditam que deveria ser estabelecido um conjunto consistente de regras a serem aplicadas nesta situação.

Sobre a culpa, em caso de violação de privacidade, a maioria (58\%) respondeu que recorreria às operadoras de telefonia móvel, independente de quem fosse a culpa. Isso
provavelmente se deve ao fato de 52\% dos entrevistados acreditarem (cegamente) que as operadoras estavam tomando as devidas precauções para manter seus
dados em segurança.

Em ambas as pesquisas, o que se nota é que falta uma preocupação de empresas/governo/desenvolvedores de aplicações quanto à perspectiva do usuário sobre o contexto geral
de uso de seus dados. Neste sentido, pode-se constatar que falta transparência, com meios esclarecidos para que o usuário possa optar por usar uma funcionalidade ou
autorize que ela seja executada (automaticamente), dentro de um escopo de exposição controlado pelo usuário a todo tempo, que seria o cenário minimamente ideal na
provisão de serviços que lidam com dados pessoais \citep{walravens2011city}.

Finalmente, o problema de uma abordagem centrada em pessoas é que humanos são entidades de comunicação/sensoriamento naturalmente passivas, em grande parte do tempo não
envolvidas no processo, aguardando algum estímulo externo que se traduza em proposição de valor em algum nível (pessoal, social, econômico, cultural, por exemplo), que
torne a troca justa \citep{lee2011building}. Com isso, para tornar pessoas em sensores efetivamente, as aplicações têm que sentí-las, suas necessidades, desejos, seu
entorno, suas interações com o meio em que estão imersas e com as pessoas à sua volta, sem requerer delas a execução de tarefas tediosas (como abrir/acessar uma aplicação
diversas vezes, preencher formulários ou pressionar botões) com o único propósito de fazê-las atuar como fonte de dados. É necessário engajá-las com aplicações que
proporcionem algum valor agregado, ajudá-las a lidar com sua rotina cotidiana e, baseado neste tipo de interação e de forma transparente, torná-las em sensores urbanos
vivos.

A visão das pessos registradas pela pesquisa retrata uma visão imediatista sobre o assunto. Entretanto, todo este cenário de privacidade se agrava quando se pensa a longo
prazo. Como já citado anteriormente, o monitoramento constante ao qual as pessoas estão expostas, aliado à evolução tecnológica atual, regada à computação em nuvem,
\textit{big data} e conceitos adjacentes, elevam o tema privacidade para um nível mais sofisticado e um tanto quanto não explorado, que se intersecta com mecanismos
legais que vêm sendo criado para dar respaldo ao cidadão comum. É sobre essa intersecção que a seção a seguir vai discutir.
