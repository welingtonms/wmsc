\section{Motivação}
\label{sec:motivation}

A população do planeta está gradativamente deslocando-se para áreas urbanas. De acordo com relatórios da \ac{ONU} e do Banco Mundial, em 1950, cerca de 30\% da população
mundial vivia em áreas urbanas; desde então, este número quase duplicou até 2014, aumentando para 54\%, com a estimativa de atingir 66\%, em meados de 2050
\citep{onu2007world, onu2011world, TWB-2013, onu2014world}.

Tamanho crescimento desenfreado traz consigo diversos problemas, no que diz respeito à provisão de serviços urbanos. Entende-se por serviço urbano qualquer ação
direcionada a suprir necessidades básicas dos cidadãos, tais como eletricidade, água potável, transporte público, segurança, saúde, educação, saneamento
básico, tratamento de resíduos sólidos, etc. A disponibilidade e efetividade destes serviços asseguram aos cidadãos uma qualidade de vida digna, reforçando a
concretização dos artigos 21, 22 e 25 da Declaração Universal dos Direitos Humanos \citep{assembly1948universal}.

% Em \citep{walravens2011city}, Walravens apresenta um exemplo simples do que aconteceria se a população mundial realmente atingisse a marca de 9.6 bilhões de pessoas, com
% os referidos 70\% alojados em áreas urbanas, já que grande parte das cidades (principalmente de países subdesenvolvidos ou emergentes) não está devidamente equipada
% para suportar tamanho crescimento, nem mesmo possui a infraestrutura necessária para gerenciar suas consequências: esse contingente seria responsável por 60-80\% do consumo anual de
% energia elétrica.

% A população urbana mundial cresceu de 746 milhões, em 1950, para 3.9 bilhões em 2014.
O principal motivo para tamanho crescimento da população urbana mundial é que, por natureza, as cidades geralmente oferecem maiores oportunidades de acesso aos serviços
públicos citados, para perfis de famílias de diferentes realidades econômicas; além disso, é nas cidades que se espera encontrar um mercado de trabalho mais amplo e
melhores condições de vida. 

Entretanto, para que essas expectativas sejam materializadas, o Departamento de Assuntos Econômicos e Sociais da \ac{ONU} \citep{onu2014world} aponta que se
faz necessário uma abordagem de planejamento e gerenciamento, na qual os serviços urbanos sejam providos de forma igualitária e eficaz, sustentados por um
conjunto vasto de informações sobre a cidade, de onde se possa extrair as necessidades a serem supridas, falhas em processos urbanos, quais medidas podem ser tomadas,
bem como as evidências dos resultados das ações estabelecidas. A grande dificuldade que se apresenta é que a grande parte das cidades - principalmente de países
subdesenvolvidos ou emergentes - não está devidamente preparada para suportar tamanho crescimento, nem mesmo possui a infraestrutura necessária para gerenciar suas consequências \citep{walravens2011city}.

É neste cenário que se estabelece o conceito de \ac{CI} (ou, em inglês, \textit{Smart City}). Apesar das diversas definições, é consenso afirmar que uma \ac{CI} emprega
tecnologia para solucionar problemas no âmbito urbano. 

No aspecto humano e social, uma \ac{CI} implica em melhorias no cotidiano dos cidadãos, com medidas implementadas ou suportadas por aparatos tecnológicos, promovendo
acesso aos serviços urbanos \citep{anthopoulos2010digital}.

No aspecto técnico, \textit{smart} refere-se à conectividade onipresente, através de uma infraestrutura sofisticada de informação e comunicação, permitindo otimização no
uso de recursos finitos, materializando princípios como capacidade de adaptação às variáveis do contexto urbano, recuperação em situações de falha,
segurança contra investidas mal intencionadas e otimização - soluções de problemas urbanos mais efetivas, melhor aproveitamento no uso de recursos e maximização de
resultados - na gestão de uma cidade, conforme discutido em \citep{nam2011conceptualizing, bowerman2000vision}; estas tecnologias devem ser adaptadas ao ambiente urbano, criando-se um
contexto adequado à participação física e virtual dos cidadãos, num processo de planejamento e gerenciamento ``das partes'' para o ``todo'', ao contrário do que é feito
atualmente \citep{Urban360-2012}.


De uma forma mais granular, uma cidade é inteligente quando utiliza \acp{TIC} de forma apropriada para prover serviços a comunidade - permitindo participação democrática
e estimulando o engajamento - melhorando a qualidade de vida, enquanto cria um ambiente mais sustentável, com investimentos de ordem social e infraestrutura moderna de
comunicação, encorajando o desenvolvimento econômico através de uma gestão integrada e eficiente \citep{nam2011conceptualizing, andreini2011scalable,
asimakopoulou2011buildings}. Neste contexto, os cidadãos teriam acesso a uma variedade de tecnologias e serviços avançados, usando qualquer dispositivo, em qualquer lugar
e a qualquer hora \citep{andreini2011scalable, Urban360-2012}.

% These technologies must be adaptable to urban environments, and the city must be suitable for digital/real participation, losing the top-down  chemes for urban planning
% and city management. The growth of services like smart mobility, smart environment and smart people is taken as main enabler of the Smart city concept.

O assunto \ac{CI} ganhou relevância nos últimos anos como solução para a crescente demanda de serviços urbanos. Aliado ao crescimento sustentável, espera-se que o uso de
\acp{TIC} supra eficientemente tanto objetivos tecnológicos quanto de negócios \citep{walravens2011city}. Porém, mais que integração de sistemas, criação de infraestrutura
ou implementação de serviços, o aspecto tecnológico assume papel de facilitador na criação de um ambiente de inovação, que requer criatividade, conectividade e
colaboração; o aspecto social possui vital importância, tanto quanto (ou mais que) o tecnológico, exigindo a compreensão da complexidade nas relações sócio-tecnológicas
do ecossistema urbano \citep{nam2011conceptualizing}.

% A set of technological requisites for smart city comprises network equipment (fiber optic channels and Wi-Fi networks), public access points (wireless hotpots, kiosks),
% and service-oriented information systems \citep{nam2011conceptualizing}.

Para que o conceito seja colocado em prática, é necessário entender a dinâmica de funcionamento de cada cidade e, para tal, a coleta de dados é uma forma promissora que
se apresenta, permitindo compreender os problemas, identificar falhas e propor melhorias e novas soluções para provisão de serviços, como sugerido em
\citep{onu2014world}.

Com o propósito de gerar informações, que por sua vez favoreçam a sustentação de negócios, uma quantidade sem precedente de dados precisa ser coletada através
de dispositivos, pessoas e sensores conectados, gerados a partir de inúmeras transações por dia. Esses dados podem ser coletados de forma ativa, nos modos tradicionais
(como o preenchimento de formulários, questionários, etc.), ou ainda de forma passiva, como subproduto de outras atividades ou por transações
\ac{M2M}\footnote{A comunicação \ac{M2M} é caracterizada pela troca de infomações entre dispositivos sem interveção humana.}, sem ser necessariamente consentido - como a
própria navegação na Internet, informações de localização obtidas a partir de dispositivos móveis (a exemplo do que foi noticiado em \citep{amadou2013google}) ou imagens
capturadas por camêras de vigilância (como colocado em evidência em \citep{sengupta2013privacy, stone2013the, amadou2013google, pincus2013many}).

Disperso no meio dos dados coletados existe uma quantidade significativa de dados pessoais, que podem ser analisados e combinados gerando ainda mais dados. Essa forma de
atuação se iguala em consequência à coleta passiva de dados, na qual o indivíduo é exposto sem conhecimento ou consentimento \citep{IIC-2012, WEF-2013, tene2012big}. O
advento de \ac{BD} \citep{chui2011big, rubinstein2012big}, que implica na consolidação do gerenciamento dessas grandes massas de dados, agrava o comprometimento da
privacidade.

Em \cite{rubinstein2012big}, fala-se sobre três dimensões que definem \ac{BD}: i) disponibilidade de dados em grande escala, coletados não somente a partir de
transações online, mas através do uso de dispositivos móveis (através de sensores embarcados ou de aplicações que permitam compartilhamento de dados), interações com
ambientes inteligentes (construídos através da Internet das Coisas) \citep{IOTSUR-2010} e a Web 2.0, que permite a criação e compartilhamento voluntários de
dados pessoais e correlatos; ii) aumento na capacidade de armazenamento e processamento dos computadores modernos, que viabiliza o modelo de computação em nuvem e; iii)
uso da capacidade computacional para armazenar e processar uma quantidade gigantesca de dados.

As características citadas acentuam a vulnerabilidade inerente aos dados pessoais dispersos na referida massa de dados, quer sejam em sua forma primitiva (dado bruto) ou
derivada (gerada a partir de algum tipo de análise), e levantam diversos questionamentos quando se discute a regulamentação de uma lei de proteção aos dados,
principalmente no que tange a distinção entre dados pessoais e não pessoais e à validade das leis sobre os dados gerados a partir de análise e mineração de dados.

Em contrapartida, Tene e Polonetsky \citep{tene2012big} discutem a necessidade de alinhar a evolução tecnológica e as realidades de negócio com os princípios de
minimização de dados e limitação no propósito de uso desses dados. Porém, esses princípios são divergentes quando trata-se de \ac{BD}, onde se prevê maximização de dados (quanto mais
dados para analisar, mais refinados e precisos serão os resultados) e a descoberta de correlações não aparentes e/ou esperadas.

Além disso, um dos problemas imediatos de \ac{BD} é o seu efeito incremental. Naturalmente, quanto maior o acúmulo de dados pessoais, mais comprometida a privacidade. A
literatura mostra que, mesmo quando as grandes corporações tentam minimizar o impacto de suas ações com algoritmos de deidentificação\footnote{Processo de reversão da
anonimização aplicada ao dado, tirando quaisquer referências ao indivíduo a quem pertencem.}, é possível executar o processo reverso e associar novamente os dados aos
respectivos indivíduos \citep{narayanan2008robust, ohm2010broken}. Ademais, uma vez o dado exposto, torna-se praticamente impossível recuperá-lo e mantê-lo novamente em
segredo. %o que compromete, por exemplo, o ``\textit{direito de ser esquecido}'' proposto na Regulamentação de Proteção de Dados da União Europeia \citep{reding2012eu}.

Outro problema é o desequilíbrio entre os benefícios de governos e corporações e dos indíviduos monitorados. Primeiro, porque geralmente serviços online são oferecidos em
forma de barganha: você cede seus dados e recebe em troca um serviço ``gratuito''; neste cenário, assim como num jogo em que um jogador sabe de antemão as cartas do
outro, governos e corporações têm em mãos mais informações sobre o indivíduo do que ele possui sobre si mesmo, facilitando a exploração de suas necessidades, gerando
vantagens unilaterais \citep{tene2012big}; segundo, o segredo de negócio das grandes corporações, ao invés de focado numa relação equilibrada de proposição de valor,
restringe-se a dados que somente elas tem posse, coletados, utilizados e mantidos em condições alheias ao conhecimento público.

Este panorama reflete a forma como nossos dados são tratados, no qual o indivíduo, seus direitos e suas preferências são mantidos em segundo plano. 

A proposta a ser descrita neste trabalho vai em direção semelhante aos princípios estabelecidos em \citep{reding2012eu} e em \citep{nist2013security} e apresenta o
\textbf{\textit{Go!SIP}}, um \textit{framework} para proteção de dados pessoais em \ac{CI}, no qual se extrapola o uso do dado por si só e permite-se o fluxo de
conhecimento útil sobre a cidade e seus habitantes, em um processo de colaboração mútua, fomentado pelo consenso e decisão informados, possibilitando a manutenção da
privacidade de dados pessoais.
