%\chapter{Cidades Inteligentes: Uma Visão Geral} % (fold)
\chapter{Fundamentação Teórica}
\label{cha:smart-cities}

\begin{quotation}[]{Ralph Waldo Emerson}
Do not go where the path may lead you,\\
Go instead where there is no path and leave a trail.
\end{quotation}

\section{Cidades Inteligentes}
\label{sec:sc-architectures}

Os aglomerados humanos que hoje conhecemos como cidades surgiram quando os antigos caçadores nômades aprenderam as primeiras técnicas agrícolas.  Com os alimentos
excedentes que essas técnicas propiciavam, não era mais necessário que todos plantassem, surgindo a possibilidade de comercialização tanto de alimentos, quanto de
ferramentas e outros bens. Com a necessidade de mão de obra para sustentar o comércio, algumas pessoas partiram do campo para esses aglomerados. 

As cidades modernas só surgiram após a  Revolução Industrial (1760) quando novas tecnologias, distribuídas em larga escala, permitiram que as cidades se expandissem e
crescessem, criando a necessidade e culminando no surgimento dos serviços urbanos que conhecemos hoje \citep{montagna2014industrial, reader2004cities,
kite2014urbanization}.
 
Sob o ponto de vista tecnológico, as cidades também sofreram diversas alterações ao longo dos anos, utilizando \acp{TIC} gradativamente, com o intuito de dar suporte aos
serviços disponibilizados, de modo que sejam mais convenientes e acessíveis aos seus cidadãos e, principalmente, possam ser providos de forma mais eficiente e eficaz.

Dentro do histórico de evolução de cidades comuns, as Cidades Digitais, a exemplo das cidades cidades AOL \citep{SA2011} e das cidades de Kyoto e Amsterdam, criadas nos
anos 90, são as primeiras criações tecnológicas reportadas na literatura em direção ao que hoje chamamos de \ac{CI} \citep{ishida2002digital}. Assim como nas Cidades
Virtuais \citep{derudder2011international}, nas Cidades Móveis \citep{anthopoulos2010digital}, nas Cidades Úbiquas \citep{gil2011experiences, lee2011building} e nas
Cidades Cognitivas \citep{mostashari2011citizens}, o objetivo era criar uma infraestrutura que possibilitasse o acesso democrático a alguns serviços, disponibilizados de
forma virtual, diminuindo a dependência da intervenção humana em processos repetitivos que poderiam ser automatizados. Estas abordagens, devido ao foco em serviços,
acabavam criando silos de serviços, segregando informações que, se compartilhadas entre departamentos e organizações, poderiam gerar insumo para implementação de
melhorias na gestão urbana.

Em seguida, esses modelos de cidades evoluíram da atenção aos serviços, para o fluxo de informação que esses serviços manipulavam. As Cidades da Aprendizagem
\citep{plumb2007learning}, as Cidades do Conhecimento \citep{anthopoulos2010digital} e as \textit{Intelligent Cities} \citep{komninos2009intelligent,
malek2009informative} visavam criar uma economia de informação baseada em ações coletivas, oferecendo infraestrutura para troca de conhecimento, como suporte à propagação
do conhecimento e ao crescimento de negócios locais, estimulando o aprendizado, o desenvolvimento tecnológico e a inovação, envolvendo pesquisa, desenvolvimento e
evolução de produtos e novas tecnologias, servindo de esteio para indústrias inovadoras. 

Até este momento, o uso da tecnologia no contexto urbano havia priorizado o estabelecimento e manutenção de negócios, fundamentados em informações obtidas através dos
serviços providos, e negligenciado o aspecto humano inerente e o benefício coletivo da tarefa, sem visar melhorias na qualidade de vida do cidadãos através dos serviços
disponibilidos. Frente à crescimento populacional apresentado na Motivação (Seção \ref{sec:motivation}) deste trabalho, a atenção ao modo como o ecossistema urbano e
suas dinâmicas impactam na qualidade de vida dos seus habitantes, assumem um carater emergencial. É neste cenário que se estabelecem as Cidades Inteligentes (CIs).

O conceito de \acp{CI} surge da necessidade de gerir os diversos problemas decorrentes do aumento da população nos grandes centros urbanos, afetando serviços
como transporte, saúde pública, gestão de redes de energia elétrica e água, saneamento básico, etc. \citep{WWAP-2009}.

De uma forma bem simplista, o gerenciamento de cada serviço sugere um monitoramento constante, suportado por mecanismos de coleta de dados. Estes dados podem ser
processados e analisados, gerando como resposta alguma ação que assegure ou auxilie a provisão de serviços urbanos em níveis satisfatórios de qualidade e efetividade.
Numa visão mais complexa, distribuição e integração são requeridos, tanto em relação aos elementos monitorados quanto à aplicação das ações necessárias; os dados devem
ser relacionados e o processamento e a análise devem levar em consideração a influência de agentes externos e de outros serviços.

A visão complexa da aplicação de gerenciamento de serviços - distribuída e integrada - requer a utilização massiva de algum tipo de sensoriamento. É a partir de um
conjunto de sistemas de monitoramento que se pode construir uma visão holística (visão sistema de sistemas) da cidade, dedicada à manutenção eficiente de seus serviços,
com o objetivo de melhorar a qualidade dos mesmos.

Especializando esta visão para um cenário onde cada objeto é provido de inteligência e/ou tecnologia suficientes para transformá-lo em um provedor/consumidor de dados, é
natural imaginar as referidas distribuição e integração, onde cada nó (objeto) possui parte da informação necessária para algum fim e tem uma responsabilidade específica
de fazer esse dado chegar até uma entidade centralizadora, capaz de processar e gerenciar os dados provenientes de diversas (eventualmente centenas ou milhares) fontes de
dado. A este conjunto de objetos agindo de forma colaborativa, na busca de um propósito comum bem definido é chamado de \ac{IoT}, que constitui um dos
fundamentos tecnológios essenciais na implementação de \acp{CI} \citep{IOTSUR-2010, Haubensak-2011}. 

Muitos dos problemas enfrentados pelas grandes cidades poderiam ser evitados, ou mesmo minimizados, se aplicado o gerenciamento de serviços, usando \acp{TIC}
\citep{Haubensak-2011, klein2008industrial}. O monitoramento do tráfego em ruas e rodovias poderia alimentar sistemas de informações capazes de redistribuir o fluxo de
veículos, através de rotas calculadas em tempo real; o monitoramento do consumo de energia elétrica de eletrodomésticos permitiria otimizar sua operação, com base nos
hábitos e necessidades dos residentes; desastres naturais poderiam ser previstos em tempo hábil para que as ações adequadas pudessem ser tomadas.
Estes são apenas alguns dos cenários nos quais a tecnologia poderia assumir um papel auxiliar em tomadas de decisões estratégicas.

A literatura contém diversos trabalhos que reportam algumas implementações de \acp{CI}, que por sua vez incluem uma quantidade significativa de tecnologias e conceitos.
Por este motivo, para embasar a temática deste trabalho, iniciou-se em uma revisão, publicada em \citep{da2013smart}, na qual buscaram-se analisar concepções de
arquiteturas para \ac{CI}, propostas em trabalhos na academia e na indústria, sob o ponto de vista dos requisitos que buscam satisfazer, a fim de se encontrar os
problemas mais abordados na área, bem como os tópicos de pesquisa mais carentes de atenção. Estes requisitos serão brevemente comentados na subseção que se segue.

\subsection{Requisitos para Cidades Inteligentes: Um \textit{Overview}}
\label{subsec:sc-requirements}
A Tabela~\ref{tab:requirements-sc-survey} sumariza os requisitos e os respectivos trabalhos estudados nos quais foram encontrados.\\

\begin{spacing}{0.2}
\begin{table}[ht]
\centering
\caption{Mapeamento Requisitos-Trabalhos \label{tab:requirements-sc-survey}}
\begin{tabular}{|p{6cm}|p{9cm}|}
	\hline
    \textbf{Requisito}  &   \textbf{Trabalhos}\\
	\hline\hline
    Monitoramento em tempo real & \citep{Components-2009, asimakopoulou2011buildings, attwood2011sccir, filipponi2010smart, hernandez2011smart, livingPlanIT, sanchez2011smartsantander}\\
	\hline
	Interoperabilidade de objetos & \citep{filipponi2010smart, hernandez2011smart, lee2011building, zygiaris2013smart, livingPlanIT}\\
	\hline
    Sustentabilidade & \citep{SETIS, Haubensak-2011, klein2008industrial, lee2011building, zygiaris2013smart}\\
	\hline
    Dados históricos & \citep{MB2-2010, lee2011building, livingPlanIT, sanchez2011smartsantander}\\
	\hline
    Mobilidade & \citep{MB2-2010, hernandez2011smart, sanchez2011smartsantander, zygiaris2013smart}\\
	\hline
    Processamento e sensoriamento distribuídos  & \citep{filipponi2010smart, andreini2011scalable, hernandez2011smart, lee2011building}\\
    \hline
    Tolerância a falhas & \citep{hernandez2011smart, nam2011conceptualizing, sanchez2011smartsantander}\\
	\hline
    Composição de serviço e gerenciamento urbano integrado & \citep{anthopoulos2010digital, nam2011conceptualizing, livingPlanIT}\\
    \hline
    Aspectos sociais & \citep{asimakopoulou2011buildings, klein2008industrial}\\
    \hline
    Flexibilidade/Extensibilidade & \citep{klein2008industrial, lee2011building}\\
    \hline
    Privacidade & \citep{IBM, livingPlanIT}\\
    \hline
\end{tabular}
\end{table}
\end{spacing}


O requisito \textbf{Monitoramento em Tempo Real} representa uma importante característica inerente ao contexto de cidades inteligentes: o monitoramento contínuo e em
tempo real. O monitoramento em tempo real é o instrumental mais valioso para o fornecimento de informações relevantes que serão utilizadas para prever fenômenos. Um
exemplo disso é o monitoramento do nível dos rios durante as temporadas de chuva. Nesta situação, a partir de um monitoramento efetivo, é possível tomar medidas para
mitigar possíveis transtornos aos cidadãos, como enchentes e a transmissão de doenças.

\textbf{Interoperabilidade de Objetos} é um requisito fundamental para a consolidação de qualquer plataforma que utilize uma gama de objetos com diferentes especificações
técnicas e protocolos de comunicação, no qual um objeto é uma abstração de sensor, atuador ou qualquer outro dispositivo que possa realizar algum tipo de computação.

O trabalho também identificou a importância da \textbf{Sustentabilidade}. Por redefinir essencialmente a dinâmica de uma cidade, propostas de implementação para \acp{CI}
precisam incluir, desde sua concepção, políticas sustentáveis. Estas políticas devem estar relacionadas aos aspectos ambiental, econômico e social de cada domínio.

Quanto ao requisito \textbf{Dados Históricos}, no contexto de \acp{CI}, todos os componentes que compõem cada domínio de uma cidade estão constantemente sendo
modificados, seja por eventos humanos, naturais ou tecnológicos. Dessa forma, todo dado captado tem potencial para compor uma informação relevante, desde que seja
agregado a outros dados, logo, torna-se substancial que as propostas contemplem mecanismos eficientes de armazenamento e consulta desses dados.

Por sua vez, a \textbf{Mobilidade} é outro requisito fundamental que deve ser explorado. Por mobilidade, entende-se toda e quaisquer tecnologias móveis, capaz de captar
informações do ambiente, passiva ou ativamente, ou atuar sobre o mesmo. A mobilidade é um dos principais aliados para a implementação do monitoramento em tempo real. Ao
considerar que 4 bilhões de cidadãos já possuem \textit{smartphones} \cite{Hall-2011}, é natural associar mobilidade ao uso destes dispositivos. Porém outros dispositivos
também podem ser utilizados, como \textit{ZigBee} e RFID (\textit{Radio-frequency identification}).

Para que sejam propostas soluções para o aumento da eficácia em serviços urbanos, é necessário que se façam aferições das características qualitativas ou quantitativas
que permeiam esses serviços, bem como dos resultados que produzem. É através de sensoriamento que se obtêm a visão computacional do ambiente urbano; quanto maior o número
de sensores e mais disperso eles estiverem, maior será o escopo abrangido pela solução proposta. A heterogeneidade dos sensores utilizados influencia na riqueza de
detalhes e na quantidade de dados que podem ser extraídos de cada cenário sendo monitorado, tornando possível a obtenção de resultados mais precisos; por exemplo,
relatórios de trânsito em determinada via podem ser melhor analisados e complementados com imagens obtidas através de câmeras instaladas nas redondezas. Além disso, 
situações que exigem a adoção imediata de medidas preventivas ou corretivas requerem processamento em tempo real, com tempo de resposta
rápido o suficiente para fundamentar as ações que devem ser executadas. Considerando quantidades massivas de dados coletados, o
suporte à esse tipo de contexto sugere a necessidade de processamento distribuído, explorando a capacidade da infraestrutura existente. Esses cenários caracterizam o
requisito \textbf{Processamento e sensoriamento distribuídos} \citep{hernandez2011smart, filipponi2010smart, andreini2011scalable, lee2011building}. 

Outro requisito identificado como importante para permitir a captação de dados, é a \textbf{tolerância a falhas} da infraestrutura subjacente, com mecanismos de controle
de fluxo, colisão e redundância devem ser inerentes à solução. Entretanto, a implementação de uma \ac{CI} não pode ser dependente da infraestrutura de \textit{cloud}, ou
seja, independente do estado da \textit{cloud}, o sistema deve continuar obtendo e armazenando os dados, inclusive atuando de forma autônoma
\citep{sanchez2011smartsantander, hernandez2011smart, nam2011conceptualizing}.

O requisito \textbf{Composição de serviços e Gerenciamento urbano integrado} parte de uma visão sistêmica, na qual ambientes urbanos são essencialmente um conjunto de
sistemas complexos, disponíveis para suprir as necessidades de seus cidadãos. Qualquer implementação de \ac{CI} que pretende dar suporte a esse tipo de sistema deveria
considerá-los como complementares na busca pelo gerenciamento urbano efetivo, ao invés de tratá-los de forma isolada. Assim sendo, serviços desenvolvidos para sustentar
tais sistemas devem ser interoperáveis, de forma que outros serviços possam reusá-los, agrupá-los ou criar uma composição a partir deles, explorando importantes aspectos
de sua correlação, ou mesmo criar uma visão holística e contextualizada da cidade, agregando informações de diferentes fontes, permitindo uma gestão urbana  mais efetiva
e integrada \citep{anthopoulos2010digital, nam2011conceptualizing, livingPlanIT}.

Tendo em vista que o propósito principal na concepção de uma cidade inteligente é o aumento na qualidade de vida de seus cidadãos, outro requisito que surgiu a partir do
estudo realizado foram os \textbf{Aspectos sociais}. Apesar do aparato tecnológico ser um dos possíveis elementos que podem ser utilizados para a implementação de
\acp{CI}, as pessoas precisam participar e serem beneficiadas pelo processo, caso contrário, todo investimento será em vão. Um exemplo disso, é a Cidade Digital de
Trikala, Grécia, que após cinco milhões de Euros gastos em manutenção de infraestrutura e 6 anos de funcionamento, a população não utilizava ou sequer tinha conhecimento
dos serviços digitais disponíveis \cite{anthopoulos2010digital}. As pessoas precisam sentir-se inclusas como parte fundamental da solução idealizada no conceito de
\ac{CI}. Para isso podem ser criadas formas de estimular e/ou retribuir esse interesse, como é caso da iniciativa \textit{Fun Theory} \cite{theFunTheory}, da Volkswagen,
na qual criam-se novas formas de se fazer tarefas repetitivas, a fim de estimular a mudança de hábitos. Além disso, os serviços devem estar disponíveis para todos os
cidadãos independente de quaisquer restrições social, física, econômica ou financeira, a tecnologia deve ser aplicada afim de trazer benefícios coletivos, e não alienar
ou elitizar uma pequena parte da população \citep{asimakopoulou2011buildings, klein2008industrial}.

A \textbf{Flexibilidade/Extensibilidade} também foi vista como um requisito relevante nas abordagens estudadas, apontando que mudanças, adaptações e extensões devem ser
previstas, quando se estabelece uma \ac{CI}. Além da inserção de novos serviços, novos tipos de sensores, diferentes tipos de dados coletados, diferentes contextos urbanos e
funcionamento independente de padrões específicos de \textit{hardware} devem ser contemplados, permitindo que possa ser adaptável a diferentes realidades
\citep{klein2008industrial, lee2011building}.

Todas as questões de manutenção de dados envolvidas na concepção de uma \ac{CI} são de suma importância. Porém, devem ser estabelecidas políticas de privacidade
esclarecendo quais dados serão capturados e o que será feito com eles. Certamente a não consolidação destas políticas é um desafio que pode impedir os cidadãos,
instituições e governo de fornecerem determinados dados críticos, atravancando o sucesso na implantação e manutenção de uma \ac{CI}. O requisito \textbf{privacidade} foi
abordado com menor profundidade/relevância em apenas dois dos trabalhos estudados, \citep{livingPlanIT, IBM}, expressando preocupação com a garantia desse direito aos
cidadãos, apesar de ambos não disponibilizarem detalhes de como implementam privacidade em suas soluções, o que impediu uma análise mais apurada do assunto no contexto de \ac{CI}.

\subsection{Considerações }

O levantamento desses requisitos permitiu que se pudessem identificar os tópicos de pesquisa mais/menos abordados no contexto de \acp{CI}, indicando
possíveis direcionamentos que o trabalho aqui apresentado poderia assumir. A escassez de trabalhos explorando o tema privacidade a oportunidade que representa, foi o que
despertou maior interesse, tanto pela interdisciplinaridade, quanto pelo potencial das possíveis soluções vislumbradas. 

O intuito de tratar o tema sob o ponto de vista de \acp{CI} deu-se pela visão de que, o uso intensivo de \acp{TIC} nas cidades, é uma tendência que representa uma
evolução na forma de gestão urbana, utilizando serviços e informações em prol da qualidade de vida das pessoas. Entretanto, como consequência dessa evolução cria-se um
ecossistema favorável ao uso desordenado de dados e informações pessoais, trazendo prejuízos para o cidadãos, anulando o efeito benéfico proposto pela tecnologia.

Na seção que se segue o tema privacidade será definido de acordo com a literatura, tendo seu significado estreitado para a conceituação que será utilizada ao longo do
trabalho.
