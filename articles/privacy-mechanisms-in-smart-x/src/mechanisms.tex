In this section we will discuss the approaches we found in the literature through an exploratory research. We tried to find any work that reported the developement of a
privacy management mechanism applied to an explicitly declared smart context. Six authors were involved in the process to avoid a biased analysis. In the following
subsections these works will be presented, ordered firstly by domain (Smart Grids, Smart Buildings and Homes, Smart Environments, Smart Spaces, Smart Health, Smart Cities
and a general category for other domains and approaches), and then according to their publication date.

\subsection{Smart Grids}

A Smart Grid is seen as ``\textit{a modernized electrical grid that uses Information and Communications Technology (ICT) to gather and act on information about the
behaviours of suppliers and consumers, in order to improve the efficiency, reliability, economics, and sustainability of the production and distribution of
electricity}"\cite{pekala1991us}. In the following lines we present approaches we found in the literature for privacy management in this domain.

% Privacy and security are hard to keep due to the dynamicity inherent to some contexts. As said previously, in smart grids, besides the data used to identify consumers,
% measured data can expose life style and usage patterns, allowing the information to be used for purposes different from what it was supposed to. On this problem,
% National Institute of Standards and Technology (NIST) published its reports \cite{NationalInstituteofStandartsandTechnology2010} addressing both security and privacy
% issues.
%  On security issues, confidentiality (for consumer's and measurement data), availability, integrity and accountability (for system audit) are cited as goal for Smart
% Grid projects. On privacy, four aspects were taken into account \textemdash personal information, personal privacy, behavioural privacy and personal communication
% \textemdash and must be met in order to mitigate risks of misuse by external agents.

% Still considering highly dynamic contexts, adaptive security allows security rules to be changed along with assets and act proactively and reactively.
% \textbf{Proactive adaptation makes it possible to reconfigure security controls} in order to mitigate high risks; Reactive actions respond to incidents and detected
% abnormalities. Similarly, following NIST recommendations, adaptive privacy aims to monitor information transmission to detect and mitigate possible threats.

In \cite{ salehie2009self}, Salehi et al. presents an approach based on adaptive security, following NIST \cite{NationalInstituteofStandartsandTechnology2010}
recommendations. Due to the highly dynamic nature of Smart grids, adaptive security allows security rules to be changed along with assets and act proactively and
reactively.
% \textbf{Proactive adaptation makes it possible to reconfigure security controls} in order to mitigate high risks.

The authors use an Adaptation Manager (AM), which monitors and analyses contextual changes in the grid and its environment at runtime, in order to detect if any privacy
or security requirement is not being met. Once a threat is detected, AM has to decide what action must be taken. Making decisions in \textbf{adaptation take into account
uncertainty and deals with multiple objectives such as risk adaptation}\footnote{Once the variables that characterize a risk have changed, security rules must also change
accordingly.}, privacy threat mitigation\footnote{Sensitivity of threatened data, history of previous violations, the obfuscation level and the ability to infer
behavioural privacy from disclosed information should be considered.} and multi-objective decision-making\footnote{Monitoring and tracking of assets, customer's behavior
and measured data should not allow personal information exposure, thus, adaptive security and privacy actions can be in conflict; techniques like theory and machine
learning may be helpful in such cases.}.

Salehie et al. yet point out that \textbf{human involvement exerts an important role in the process} of adaptation, changing rules according to his own preferences when
some security or privacy issue is detected: instead of being warned about what action was taken, he would be warned about the threat in order to change the adaptation
behaviour offline.

In \cite{Fhom2010}, Fhom et al. describes a privacy manager agent, applied to the smart grid context. Two scenarios were considered, namely dynamic metering and tariff
schemes for distributed energy resources. Such scenarios would be implemented in a fully automated deployment in a future Smart Grid, avoiding residual information to
reveal energy usage patterns and details about daily routines. Since inputs of these processes are personal information or other type of data directly related to
consumers' usage habits and activities in homes, offices, or critical business data, privacy concerns emerged related to the misuse or uncontrolled disclosure of such
private data.

The proposal defines several building blocks designed to provide privacy functions and protocols, based basically on three key privacy requirements:
\textit{consumer empowerment}, \textbf{where customer must be notified about the kind of information and purpose for what it is being captured; the collected data cannot
be consumed without customer consent and preferences, besides it is possible to verify if such preferences were not violated}; \textit{data protection}, which comprehends
\textbf{confidentiality and integrity} during sensitive data handling (collection, storage, transport and processing), and \textbf{data minimization}, where customer's
personal data have to be collected or processed only when necessary, to meet well stated purposes.
\textbf{Double-side policies specification} allow that service provider also express how customer's information will be handled. \textbf{The interest matching} is
automatically applied as soon as the data is made available.

In \cite{Bohli2010c}, the authors point the adoption of smart electricity meters as a motivation for customers to save energy, \textbf{showing them their electricity
consumption}. The problem is that, using this information, a supplier can infer customer's habits, since the consumption profile differs between devices. Data aggregation
is used, so the energy provider has access only to the current overall consumption of electricity by its customers, or specific groups of customers, as well as the sum of
a customer's electricity consumption during a billing period.

The work proposes a privacy model for Smart Metering based on requirements for privacy protection, allowing an electricity supplier to receive aggregated information and
a method for measuring the degree of privacy that a smart metering application can provide, based on a guessing cryptographic game.

Gong \& Li \cite{Gong2011b} claim that the adoption of environmental-friendly power generations makes power systems more dynamical, which implies in more frequent reports
by the smart meters, with intervals reduced to few seconds or less, potentially exposing user's habits. \textbf{They proposed a data disguise approach, that emulates the
transmission pattern to disguise user presence at home}.

Smart metering is also the subject tackled in \cite{sankar2012smart}. The work aims to determine load consumed using a hidden Markov model, defined by the underlying
appliances' state. The authors defend an abstract privacy model, assuming that ``\textit{technologic specific solution may not provide the same privacy assurance in the
future}'', besides that, what today we take as simply measurement data, in a not-that-far future may be used to infer personal information in ways unknown in the present.

The theoretical framework proposed allows \textbf{quantifying the utility \textit{vs.} privacy trade-off for smart meter data}. An information leakage model is used to
\textbf{ensure the minimum information possible about a measurement, preserving the utility of the data}. Thus, observed characteristics that suggest human activities are
eliminated, while keeping useful energy consumption information.

According to \cite{Salehie2012a}, security and privacy requirements in Smart Grids, although extensively focused on the design phase, usually fails, is incomplete or no
longer is satisfied once in operation; besides architecture and design decisions, chosen technologies could have implications as well. That is why the authors suggest
adaptive software models and technologies, since they address some of these issues by handling uncertainty and automating monitoring, analysis, decision-making and the
\textbf{application of security controls}.

\subsection{Smart Building and Home}

Through the control and integration of different appliances in a residence or building, it is possible to create a pleasant ambient, responding to inconstancies or
preferences of dwellers, establishing what we can call smart homes \cite{li2008rfid}.

Privacy sensitive location information in smart building is the subject dealt in \cite{Boyer2006}, where is presented a ``\textit{model, architecture, and case study for
distributing location data from a building automation system infrastructure to users in a way that reasonably addresses privacy concerns}''.
The work proposes a Location Information System, where \textbf{a system allows users and managers to control and learn information about tracked people and objects} using
information generated from a Building Automated System (BAS). The study case is based on the Siebel Center BAS, Urbana, Illinois, in which is monitored the use of card
readers to gain access to offices throughout the building. This information is aggregated to data collected from occupancy and door-state sensors, allowing building
managers to improve maintenance functions and respond to security incidents.

A privacy aware infrastructure for Smart Home is proposed in \cite{bagues2007sentry}. Since context-aware service provision lays on sensors and tracking devices to
provide service personalization, privacy becomes a serious concern. The work describes Sentry@Home, a user-centric framework, ``\textit{seamlessly embedded into the smart
home software infrastructure, which allows the home itself act as a privacy proxy for a tracked individual}''.

The Sentry@Home, comprises five functional elements, as follows:
\begin{inparaenum}[1)]\item the Sentry Registry, which tracks the availability of people's context and provides the pointer to the appropriate Sentry@Home service
instance; \item the Sentry instance, \textbf{which manages the context disclosure of a target} to third parties; \item the Context Handler acts as an interface between
sentry instance and service registry, publishing the sentry as service, offering contextual information; \item the Sentry Manager Interface, \textbf{enables user
interaction} with the privacy framework\item the Noise Module, implements transformations and white lies to hide or disguise user's context or identity\end{inparaenum}.

The framework \textbf{avoids constant user interaction providing means to automate user's consent based on flexible and adaptable privacy preferences, gives explicit
control over which data is disclosed when, how, to whom, and with which constraints}. Additionally, \textbf{user can use noise or ambiguity injections into information}.

In \cite{mouratidis2007integrating}, the System Engineering for Security and Dependability (SERENITY) project addresses privacy issues on smart home privacy by employing
security patterns, providing novice users with the SERENITY Security \& Dependability patterns package. This package comprises expert-validated security solutions and
tested plug-and-play deployable implementations.
% So, some security requirements raised by the authors are non-repudiation, service availability, access control, integrity, confidentiality, privacy and reliability. The
% chosen approach to fulfill those requirements was security pattern.

The solution was implemented using Service Oriented Architecture. To provide confidentiality, an \textbf{authorization mechanism was implemented}; Logging capabilities
were implemented through a \textbf{Policy Enforcement Point, facilitating audits in case of privacy violation}; both mechanisms represent security patterns. Additionally,
an Integration Scheme is provided, responsible for ensuring confidentiality for distributed resources, implemented as a combination of the previously mentioned security
patterns.

The work described in \cite{Moncrieff2008b} presents a scheme to encapsulate a privacy management policy within methods used to control access to the gathered data. The
proposed framework implements privacy within a smart house environment, providing an interface between the privacy implementation and the resident, \textbf{determining
dynamically what data an observer can access} given the context of the environment. \textbf{User preferences define privacy level}, \textbf{minimizing intrusion into
dwellers life}, while smart home applications have enough information to perform their duties satisfactorily.

Initially, it is necessary to collect contextual information (who, where, how, what, why) about the environment. After that, context must be interpreted and required
measures must be established. If no privacy measure is required, then the data is sent to a consumer application; otherwise, \textbf{a ruler filter determines an
appropriate level of privacy using a rule-based approach}, incorporating predefined preferences. Then, \textbf{a data filter will use some data hiding techniques to
filter or obscure the data} in order to implement the appropriate data access level; finally, \textbf{the individuals being monitored will be able to adjust the privacy}
filter component (if needed) based on the information provided by the \textbf{feedback and control component}.

\subsection{Smart Environment}

A Smart Environment can be considered as a materialization of what was said by Mark Weiser in \cite{weiser1999origins}\textemdash a physical world widely observed by
sensors, handled by actuators and other ICT components, \textit{embedded seamlessly in the everyday objects of our lives and connected through a continuous network}. The
following paragraphs will describe privacy approaches suitable for such context.

In \cite{Heerde2006b}, Heerde et al point that asymmetric information is the heart of the privacy problem in ubiquitous computing; when this happens, there is an
asymmetry between the information knowledge of donor and collector. Since different types of sensors can be spread in a given environment, capturing data without people
awareness and consent, Life-Cycle Policies is the approach chosen by the authors, in which the \textbf{data stored degrades progressively according to the policy}, when
specific conditions are satisfied (\textbf{these policies are specified by the user}). Events are described by attributes, used to represent them graphically in an
n-dimensional space, where each dimension represents the accuracy of an attribute of the original data tuple. The final state of an event triggers a value deletion or
contains a completed degraded value.

Smart environments are also the focus in the architecture proposed in \cite{Lioudakis2007c}. A distributed middleware defines a secure domain for privacy sensitive
information; whenever information falls into it, a \textbf{police-based access rights mechanism} sets a barrier to information consumption on behalf of services and
monitoring mechanisms, acting as a proxy to the flow of personal data.

% Since precompiled policies do not enable user to handle complex information in not previewed situations, B\"unnig \cite{bunnig2009smart} proposes an ad-hoc disclosure
% system for a more dynamic and intuitive privacy management.

% User's disclosure behavior was studied in order to comprehend how users correlate situations to disclosed information; specifically, how disclosure-relevant data is
% determined and in which level of abstraction, with what accuracy a system can reproduce a human behavior in such situations and % how a user would interact with a
% system with such capabilities. A testbed was developed to evaluate how users dealt with the flow of personal information in typical services found in smart
% environments. These studies provided the needed basement for the development of the aforementioned ad-hoc disclosure system.

A more simplistic approach is shown in \cite{lupiana2010strictly}. Human users were associated to their Bluetooth-enabled mobile devices or Radio-Frequency IDentification
(RFID) cards; identifiable locations were associated with unique IDs of their embedded sensors, in this case, Bluetooth antennas and RFID readers. When a sensor detected
a device, it meant that a particular user was currently present in a specific location, allowing an application to give the appropriate support when required.
\textbf{This approach reduces the amount of Personal Identifiable Information to be collected}, according to the authors, increasing the chance of keeping user privacy,
as well as acceptability of smart environments.

Personal territory refers to a personal space demarcated by physical boundaries, such as walls and doors of a room; with the realm of ubiquitous computing and smart
environments, personal territory shifts to a virtually extension of those boundaries, which means that not only entities with physical access can interfere in a personal
territory, but also everyone/everything who has virtual access to it.
In this context K\"{o}nings et al. \cite{konings2010towards} propose a user-centric model for territorial privacy that encompasses physical and virtual observers. The
model enable the definition of boundaries including physical and virtual territorial, as well as which entities are allowed to participate in the private territory and
how. \textbf{Communication channels are established between the desired observers}, generating several virtual territories over the same physical one.

Heinroth \& Minker \cite{Heinroth2011} claim that intelligent environment's autonomous nature, accessibility to large amounts of personal information and its seamless
immersion in our everyday environments raise several privacy issues for residents. The work describes ATRACO's Privacy enforcement mechanism is based on policy matching
and access control mechanisms.  \textbf{User's preferences are represented as privacy policy} ontologies, describing
\textbf{under which conditions and context personal information can be handled}
or \textbf{interaction can be started during user's activity}. When a privacy event occurs, ATRACO's privacy manager retrieves related policies and contextual information
to perform reasoning and provides the results to the corresponding privacy controller, which will perform further enhancing mechanisms based on the results. If any
conflict occurs during the reasoning process, \textbf{the user can be requested to establish a resolution, such as modifying  policies or adding policy exceptions}.

\subsection{Smart Space}

According to the literature, Smart Spaces\textemdash the minimal physical unit to contain users \textemdash are intelligent spaces which consist of heterogeneous devices
(sensors, computers, user devices, etc.) able to cooperate dynamically\cite{suomalainen2011security}, and allow users to interact with them and provide service for them
\cite{Cho2004}. Following, we will talk about privacy management mechanisms applied to this domain.

In \cite{Cho2004}, the authors present a user location privacy protection mechanism for Smart Spaces (SS). When \textbf{user's identifier attributes and preferences} fall
under personal private data, a Privacy Protection Space takes the responsibility for preventing user's private information from being exposed. \textbf{A non-intrusive
authentication facility is provided} and, once authenticated, the user is allowed to consume several services, \textbf{interacting with the space through direct or
indirect negotiation} (when user's device negotiates with the Space on his behalf).

The work treats specifically data related to location. In order \textbf{to minimize user's intrusion and to provide invisible and proactive services}, SS itself must
collect user location information, which may incur in privacy violation due to misuse or abuses. To solve this, \textbf{user can manage her/his location information and
specify exposure rules for users located around}.

The work presented in \cite{Maisonnasse2006} proposes a perceptual approach based on interaction between humans and objects, addressing user and his relationships as the
best way of guaranteeing privacy. The work quantifies relationships specifying who pays attention to whom, assuming that attention selects which objects are present in
one's mind; relations are quantifiable through a number that represents the strength of links between users and objects. The framework can delimit privacy by establishing
what elements are common and what are specific among different users.
The attention is calculated as a linear combination of internal and external factors. Each person, or object, has a mass which allows computing the attraction vector of
each person towards the people and objects nearby using the gravitational model. When someone's attention is detected to be focused on some resource previously set as
private, \textbf{a predefined action is triggered to avoid privacy violation}.

In \cite{Liampotis2009b} is presented a privacy framework for self-improvement of Personal Smart Spaces (PSSs); PSSs are a set of user-centric services owned, controlled
or administered by a single user, provided within a dynamic space of connectable devices. These services compose a ubiquitous environment in which devices are placed,
seamlessly retrieving user data and establishing his profile without notice and consent. The risk of data exposure in such context is very high.

The framework proposed is divided as follows:\begin{inparaenum}[1)]\item The \textbf{Identity Manager handles multiple digital identities of PSS owners} and selects the
applicable identity for each transaction; \item The \textbf{Privacy Management handles privacy policies and preferences}, evaluates preferences and generates policies to
be used in the \textbf{privacy negotiation process} \item The Trust Management block is responsible for establishing the trust concept within the framework, handling
(re)assessment of trust between pair of PSS, \textbf{re-evaluation based on history of interaction} and, finally, \textbf{inferring trust level when no relation was
previously established}.\end{inparaenum} The trust concept discussion is beyond the scope of this work.

Contemporary service models promise personalized service to all users anytime and anywhere \cite{Oyomno2011a}. To a service be satisfactorily personalized, Service
Providers (SP) need to gather enough information about the user in order to provide a more sophisticated experience and more value-aggregated information. Such amount of
information raises several privacy concerns among Service Consumers (SC).

Regarding such scenario, Oyomno et al. \cite{Oyomno2011a} \textbf{propose a privacy policy enforcement} system for transparent services able to change dynamically the use
of \textbf{privacy policies and respect user's privacy preferences}, aiming to ``\textit{standardize the representation and negotiation of personal information exchanges
between service consumers and smart space service providers}''. SCs can define disclosure policies and determine what personal information can be shared with what SPs.
The personal information handling is overseen by a privacy policy enforcement authority. The work also deals with the privacy invasiveness, \textbf{computed taking into
account sensitivity of the personal information solicited} and \textbf{how such information is going to be handled}.

\subsection{Smart Health}

Smart health is seen in the literature as a solution to make easier medical monitoring to individuals whose condition requires constant care, through a health monitoring
and healthcare service delivery \cite{townsend2011privacy}. In the following paragaprahs we will talk about how proposed architectures for such domain handle privacy
concerns.

In \cite{Busnel2008b} is described a healthcare assistance for smart homes, focused on attending a growing population of elders, susceptible to health injuries which can
lead toward loss of autonomy and greater fragility, with a consequent reduction in their quality of life. The authors propose the employment of security patterns to
provide the required security solutions for applications developed for such domain. A security pattern describes a particular recurring security issue that arises in
specific contexts and presents a well-proven generic solution for it.

Guennoun and El-Khatib \cite{Guennoun2009a} present a \textbf{context-based access control} architecture to fulfill the security requirements of protecting medical data,
lifted from subject to context-centric approach, in which access authorization would be decided taking into account the identity of the requester and the medical
condition (context) of the owner.

The architecture has three main components: a) the context and health data collection manager \textemdash responsible for collecting context and health information from
the occupants of a smart home; b) the \textbf{Context-based access control manager}; c) the database system, composed by a health data, a context data and a context based
access repositories.

\subsection{Smart City}

Smart cities are, essentially, cities were ICT is applied to help solve, mitigate, foresee or minimize the impacts of problems in urbans services, in order to improve
quality of life of citizes. Here we will take a look at how privacy issues are handled in this context.

In \cite{Lioudakis2009}, the authors exploit personal data protection immersed in an Information City. Thus, the work proposes a Privacy Broker, formed by three high
level entities \textemdash citizens, data collectors and Privacy Authority \textemdash that compose a privacy domain ``\textit{in which personal data handling is subject
to both legislative requirements regarding privacy and user privacy preferences}''. To \textbf{allow user's preferences specification}, a proprietary XML-based language
is used to describe privacy preferences in order to regulate data management regarding disclosure permissions and level of disclosure, obligations, constraints of time
and space, data processing and dissemination. \textbf{Privacy agents ensure that privacy preference and policies are being applied by the service providers}, in
accordance with authorities' laws and user's preferences.

\subsection{Other domains and approaches}

% In \cite{vagts2009privacy} is proposed NEST, a framework to ensure privacy in Smart surveillance systems. Opposite to traditional surveillance systems, which perform a
% sensor-oriented approach, NEST architecture opts for a task-oriented approach. Any usage of a resource and each processing step are assigned to a concrete surveillance
% task. According to the authors, this approach allows a more efficient resource usage and offers great possibilities for enhancement of privacy.
%  Surveillance relevant data is obtained from raw data collected by sensors and fused into the Object-Oriented World Model \cite{bauer2009object}, responsible for
% privacy enforcement along with the Privacy Manager.
%  The Privacy Manager is composed by the following components:\begin{inparaenum}[1)]\item the Anonymizer removes irrelevant information from data collected by sensors,
% besides anonymizing this data; \item since any information is coupled with digital rights, the Digital Rights Manager ensures that data will be used only in a specific
% context and during execution of a corresponding task; \item an Identity Manager, that manages all objects and their entities; \item a Privacy Enforcement (Law and Fair
% Information Principles) which comprises specific location laws and the Fair Information Principles, based on the same principles presented in \cite{oecd2002oecd} \item
% a Subject Interaction, through which observed users can manage their personal data.\end{inparaenum}
In addition to the domais we studied here, other smart domains, such as Smart Surveillance \cite{vagts2009privacy} and Smart Vehicles \cite{hubaux2004security} may have
their own privacy mechanisms.

Besides the privacy mechanisms studied in this survey, some others can be studied to reinforce the implementation of privacy in smart contexts, such as anonymous
credential \cite{Cheung2011a}, 3rd party escrow \cite{Efthymiou2010b}, Pseudonyms \cite{roduner2003citizen}, Gateway \cite{Fhom2010}, Load Signature Moderator
\cite{kalogridis2010privacy} and privacy preserving authentication schemes \cite{Chim2011}, \cite{Siddiqui2012}.

After having studied all the described approaches, we have raised some common privacy requirements, which we will present in the following section.
